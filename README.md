Simple program that retrieves the TFL bicycle hire live feed every 10 minutes and stores it in a local XML file

Usage
=====

Running without any arguments will simply output the XML files in the current directory.
Can also run by passing a command line argument representing the (absolute or relative) file path to the output
directory where you want the files to be downloaded