package tflbikedownloader.channels;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import tflbikedownloader.domain.exceptions.XmlUnmarshalException;
import tflbikedownloader.domain.tfl.BikeStationStateList;
import tflbikedownloader.domain.weather.WeatherState;

import java.net.MalformedURLException;

/**
 * Main class
 * <p>
 * Created by Gabriel on 10/15/2014.
 */
public class TflBicycleDataDownloaderEntryPoint {
    private static final int DOWNLOAD_INTERVAL_MILLIS = 10 * 60 * 1000; // 10 minutes

    /**
     * @param args args[0] (optional) = path of the directory where to store the files.
     *             Default value is current directory
     */
    public static void main(String[] args) throws MalformedURLException {
        ApplicationContext ctxt = new ClassPathXmlApplicationContext("tflbikedownloader/context.xml");

        XmlDocumentUnmarshaller<BikeStationStateList> bikeUnmarshaller = (XmlDocumentUnmarshaller<BikeStationStateList>) ctxt.getBean("liveCycleHireFeedUnmarshaller");
        XmlDocumentUnmarshaller<WeatherState> weatherUnmarshaller = (XmlDocumentUnmarshaller<WeatherState>) ctxt.getBean("weatherFeedUnmarshaller");

        TflJdbcTemplate databaseWriter = (TflJdbcTemplate) ctxt.getBean("databaseTemplate");

        while(true) {

            try {
                System.out.println("Retrieving news feeds");

                BikeStationStateList bikeStationStateList = bikeUnmarshaller.unmarshal();
                WeatherState weatherState = weatherUnmarshaller.unmarshal();

                System.out.println("Got the feeds");
                System.out.println("Inserting feeds in database");

                databaseWriter.writeToDatabase(bikeStationStateList, weatherState);

                System.out.println("Inserted feeds in database successfully");

                Thread.sleep(DOWNLOAD_INTERVAL_MILLIS);
            } catch (InterruptedException ex) {
                return;
            } catch (XmlUnmarshalException ex) {
                System.err.println(ex.getMessage());
            } catch (DataAccessException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }
}
