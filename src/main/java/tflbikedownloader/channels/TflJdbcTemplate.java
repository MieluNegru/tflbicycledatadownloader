package tflbikedownloader.channels;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import tflbikedownloader.domain.tfl.BikeStationState;
import tflbikedownloader.domain.tfl.BikeStationStateList;
import tflbikedownloader.domain.weather.WeatherState;

import javax.sql.DataSource;

/**
 * Bean handling insertion of TFL and weather data to the database
 * <p>
 * Created by Gabriel on 10/25/2014.
 */
public class TflJdbcTemplate {

    private DataSource dataSource;
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * Write a new TFL live cycle hire feed update to the database, along with weather data
     * corresponding to the feed's timestamp
     *
     * @param bikeStationStates the list of unmarshalled station states from the TFL live bike hire feed
     * @param weatherState      the unmarshalled data from the weather XML feed
     */
    public void writeToDatabase(BikeStationStateList bikeStationStates, WeatherState weatherState) {
        // first get the date of the TFL feed's update. This will be also used when inserting WeatherState,
        // as the weather state's timestamp may be slightly different, breaking foreign key relationships

        long feedUpdateTimestamp = bikeStationStates.getTimestamp();

        // now, insert the weather data into the database
        insertWeatherState(weatherState, feedUpdateTimestamp);

        for (BikeStationState stationState : bikeStationStates.getBikeStationStates()) {
            insertBikeStationState(stationState, feedUpdateTimestamp);
        }

    }

    // insert weather state data into database
    private void insertWeatherState(WeatherState weatherState, long feedUpdateTimestamp) {
        String sql = "INSERT INTO weatherState(" +
                "timestamp, sunrise, sunset, temperatureValue, temperatureMin, temperatureMax, " +
                "humidityValue, pressureValue, windSpeedValue, windDirectionValue, cloudValue, " +
                "precipitationMode, weatherNumber) " +
                "VALUES(:timestamp, :sunrise, :sunset, :temperatureValue, :temperatureMin, :temperatureMax, " +
                ":humidityValue, :pressureValue, :windSpeedValue, :windDirectionValue, :cloudValue, " +
                ":precipitationMode, :weatherNumber);";

        long sunrise = weatherState.getCity().getSunTime().getSunriseTime().getTime();
        long sunset = weatherState.getCity().getSunTime().getSunsetTime().getTime();

        double temperatureValue = weatherState.getTemperatureNode().getValue();
        double temperatureMin = weatherState.getTemperatureNode().getMinimum();
        double temperatureMax = weatherState.getTemperatureNode().getMaximum();

        double humidity = weatherState.getHumidity().getValue();
        double pressure = weatherState.getPressure().getValue();
        double windSpeed = weatherState.getWind().getSpeed().getValue();

        int windDirection = (int) weatherState.getWind().getDirection().getValue();
        int clouds = (int) weatherState.getClouds().getValue();

        String precipitationMode = weatherState.getPrecipitation().getMode();
        int weatherNumber = weatherState.getWeather().getNumber();

        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("timestamp", feedUpdateTimestamp);
        params.addValue("sunrise", sunrise);
        params.addValue("sunset", sunset);
        params.addValue("temperatureValue", temperatureValue);
        params.addValue("temperatureMin", temperatureMin);
        params.addValue("temperatureMax", temperatureMax);
        params.addValue("humidityValue", humidity);
        params.addValue("pressureValue", pressure);
        params.addValue("windSpeedValue", windSpeed);
        params.addValue("windDirectionValue", windDirection);
        params.addValue("cloudValue", clouds);
        params.addValue("precipitationMode", precipitationMode);
        params.addValue("weatherNumber", weatherNumber);

        jdbcTemplate.update(sql, params);
    }

    /* insert bike station state data into database.
     * This is a 2-step process:
     *
     * Step 1. Insert data related to bike stand in the bikeStation table. This is the
     * data that doesn't change from one reading to another (i.e. id, coordinates, etc).
     * This insertion should only occur once for bike station.
     *
     * Step 2. Insert data related to this particular state in the bikeStationState table.
     * This data will be different from one update to another (i.e. number of available bikes)
     */
    private void insertBikeStationState(BikeStationState bikeStationState, long stateTimestamp) {
        // first insert the constant bikeStation data in the bikeStation table
        if (!existsBikeStation(bikeStationState.getId())) {
            insertBikeStation(bikeStationState);
        }

        String sql = "INSERT INTO bikeStationState(stationId, timestamp, installed, locked, temporary, bikeNumber, emptyDocksNumber, docksNumber)" +
                "VALUES(:id, :timestamp, :installed, :locked, :temporary, :bikesNo, :emptyDocksNo, :docksNo)";

        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("id", bikeStationState.getId());
        params.addValue("timestamp", stateTimestamp);
        params.addValue("installed", bikeStationState.isInstalled());
        params.addValue("locked", bikeStationState.isLocked());
        params.addValue("temporary", bikeStationState.isTemporary());
        params.addValue("bikesNo", bikeStationState.getBikeNumber());
        params.addValue("emptyDocksNo", bikeStationState.getEmptyDockNumber());
        params.addValue("docksNo", bikeStationState.getTotalDockNumber());

        jdbcTemplate.update(sql, params);
    }

    private boolean existsBikeStation(int stationId) {
        String sql = "SELECT COUNT(*) AS stationCount FROM bikeStation WHERE stationId = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", stationId);

        int stationCount = jdbcTemplate.queryForObject(sql, params, Integer.class);
        return stationCount > 0;
    }

    private void insertBikeStation(BikeStationState bikeStationState) {
        String sql = "INSERT INTO bikeStation(stationId, name, terminalName, latitude, longitude, installDate, removalDate) " +
                "VALUES(:id, :name, :terminal, :lat, :long, :install, :removal)";

        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("id", bikeStationState.getId());
        params.addValue("name", bikeStationState.getName());
        params.addValue("terminal", bikeStationState.getTerminalName());
        params.addValue("lat", bikeStationState.getLatitude());
        params.addValue("long", bikeStationState.getLongitude());

        long installTimestamp = bikeStationState.getInstallDate();
        params.addValue("install", installTimestamp == 0 ? null : installTimestamp);

        long removeTimestamp = bikeStationState.getRemovalDate();
        params.addValue("removal", removeTimestamp == 0 ? null : installTimestamp);

        jdbcTemplate.update(sql, params);

    }

}
