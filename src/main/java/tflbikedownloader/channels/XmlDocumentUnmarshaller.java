package tflbikedownloader.channels;

import tflbikedownloader.domain.exceptions.XmlUnmarshalException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class responsible with unmarshalling a XML feed into the generic parameter objectType
 * <p>
 * Created by Gabriel on 10/18/2014.
 */
public class XmlDocumentUnmarshaller<T> {

    private final Class<T> objectType;
    private final URL feedUrl;

    /**
     * Create a new instance that reads from the feed corresponding to the specified URL
     *
     * @param feedUrl string representing the URL of the XML document to read
     * @param objectType the type of the object to unmarshal the XML into
     */
    public XmlDocumentUnmarshaller(String feedUrl, Class<T> objectType) {
        try {
            this.feedUrl = new URL(feedUrl);
            this.objectType = objectType;
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: malformed URL injected in XmlDocumentUnmarshaller bean. The malformed URL: " + feedUrl);
        }
    }

    @SuppressWarnings("unchecked")
    public T unmarshal() {
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(objectType);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            return (T) unmarshaller.unmarshal(feedUrl.openStream());

        } catch (JAXBException e) {
            throw new XmlUnmarshalException("Error unmarshalling XML feed", e);
        } catch (IOException e) {
            throw new XmlUnmarshalException("Error opening input stream to specified URL resource: " + feedUrl.toString());
        }
    }
}
