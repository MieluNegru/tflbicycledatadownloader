package tflbikedownloader.services.file;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class used to generate XML file names that don't exist yet inside a specified directory
 * <p>
 * Created by Gabriel on 10/15/2014.
 */
public class FileNameGenerator {
    private final DateFormat dateFormat;
    private File parentDirectory;
    private String baseName;
    private String extension;

    /**
     * Create new file name generator
     *
     * @param baseName        base name to use when generating file names. For example, for baseName="foo"
     *                        and extension = "xml", the generated file name will be "foo-2014-10-15-1.xml"
     * @param extension       the extension of the file name
     * @param parentDirectory directory where you want to store the files with the generated name
     *                        each newly-generated file name will not exist in that directory
     */
    public FileNameGenerator(String baseName, String extension, File parentDirectory) {
        if (!parentDirectory.isDirectory())
            throw new IllegalArgumentException("parentDirectory argument " + parentDirectory + " is not a directory");

        this.baseName = baseName;
        this.extension = extension;
        this.parentDirectory = parentDirectory;

        this.dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }

    /**
     * Get a new file name that does not exist in {@code parentDirectory}
     *
     * @return the new file. Its name is in the format baseName-yyyy-MM-dd-fileNumber.xml
     */
    public File nextFile() {
        // use today's date for the file name
        Date date = new Date();
        int fileNumber = 1;

        String generatedFileName;
        do {
            generatedFileName = getFileName(date, fileNumber++);
        } while (fileAlreadyExists(generatedFileName));

        return new File(parentDirectory, generatedFileName);
    }

    // ascertains if parentDirectory already contains a file with the specified name
    private boolean fileAlreadyExists(String fileName) {
        return new File(parentDirectory, fileName).exists();
    }

    // utlity method for getting a file name that would be generated for date and fileNumber
    private String getFileName(Date date, int fileNumber) {
        return String.format("%s-%s-%d.%s", baseName, dateFormat.format(date), fileNumber, extension);
    }
}
