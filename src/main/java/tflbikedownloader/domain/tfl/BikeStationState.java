package tflbikedownloader.domain.tfl;

import javax.xml.bind.annotation.XmlElement;

/**
 * Domain class encapsulating the data related to the state of a TFL bicycle docking station.
 * <p/>
 * Used when unmarshalling the live cycle hire update XML feed
 * <p/>
 * Created by Gabriel on 10/17/2014.
 */
public class BikeStationState {

    private int id;

    private String name;
    private String terminalName;

    private double latitude;
    private double longitude;

    private boolean installed;
    private boolean locked;
    private boolean temporary;

    private long installDate;
    private long removalDate;

    private int bikeNumber;
    private int emptyDockNumber;
    private int totalDockNumber;

    public int getId() {
        return id;
    }

    @XmlElement(name = "id")
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public String getTerminalName() {
        return terminalName;
    }

    @XmlElement(name = "terminalName")
    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public double getLatitude() {
        return latitude;
    }

    @XmlElement(name = "lat")
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @XmlElement(name = "long")
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isInstalled() {
        return installed;
    }

    @XmlElement(name = "installed")
    public void setInstalled(boolean installed) {
        this.installed = installed;
    }

    public boolean isLocked() {
        return locked;
    }

    @XmlElement(name = "locked")
    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public boolean isTemporary() {
        return temporary;
    }

    @XmlElement(name = "temporary")
    public void setTemporary(boolean temporary) {
        this.temporary = temporary;
    }

    public long getInstallDate() {
        return installDate;
    }

    @XmlElement(name = "installDate")
    public void setInstallDate(long installDate) {
        this.installDate = installDate;
    }

    public long getRemovalDate() {
        return removalDate;
    }

    @XmlElement(name = "removalDate")
    public void setRemovalDate(long removalDate) {
        this.removalDate = removalDate;
    }

    public int getBikeNumber() {
        return bikeNumber;
    }

    @XmlElement(name = "nbBikes")
    public void setBikeNumber(int bikeNumber) {
        this.bikeNumber = bikeNumber;
    }

    public int getEmptyDockNumber() {
        return emptyDockNumber;
    }

    @XmlElement(name = "nbEmptyDocks")
    public void setEmptyDockNumber(int emptyDockNumber) {
        this.emptyDockNumber = emptyDockNumber;
    }

    public int getTotalDockNumber() {
        return totalDockNumber;
    }

    @XmlElement(name = "nbDocks")
    public void setTotalDockNumber(int totalDockNumber) {
        this.totalDockNumber = totalDockNumber;
    }
}
