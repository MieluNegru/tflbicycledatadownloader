package tflbikedownloader.domain.tfl;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * List of {@link BikeStationState} objects corresponding to a live update for a certain timestamp
 * <p>
 * Created by Gabriel on 10/17/2014.
 */
@XmlRootElement(name = "stations")
public class BikeStationStateList {

    private long timestamp;
    private List<BikeStationState> bikeStationStates;

    public long getTimestamp() {
        return timestamp;
    }

    @XmlAttribute(name = "lastUpdate", required = true)
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public List<BikeStationState> getBikeStationStates() {
        return bikeStationStates;
    }

    @XmlElement(name = "station")
    public void setBikeStationStates(List<BikeStationState> bikeStationStates) {
        this.bikeStationStates = bikeStationStates;
    }
}
