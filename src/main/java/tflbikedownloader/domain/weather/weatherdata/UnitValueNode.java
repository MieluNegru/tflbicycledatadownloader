package tflbikedownloader.domain.weather.weatherdata;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Java bean used for unmarshalling data from XML node in the format <tagName name="foo" value="bar" />
 * <p>
 * Created by Gabriel on 10/20/2014.
 */
public class UnitValueNode {

    private double value;
    private String unit;

    public double getValue() {
        return value;
    }

    @XmlAttribute(name = "value")
    public void setValue(double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    @XmlAttribute(name = "unit")
    public void setUnit(String unit) {
        this.unit = unit;
    }
}
