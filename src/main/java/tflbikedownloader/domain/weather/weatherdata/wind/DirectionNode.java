package tflbikedownloader.domain.weather.weatherdata.wind;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * The {@code DirectionNode} class represents wind direction
 * <p>
 * Created by Gabriel on 10/20/2014.
 */
public class DirectionNode {

    // direction of the wind. 0: north; 90: east; 180: south; 270: west
    private double value;

    private String code;
    private String name;

    public double getValue() {
        return value;
    }

    @XmlAttribute(name = "value")
    public void setValue(double value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    @XmlAttribute(name = "code")
    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    @XmlAttribute(name = "name")
    public void setName(String name) {
        this.name = name;
    }
}
