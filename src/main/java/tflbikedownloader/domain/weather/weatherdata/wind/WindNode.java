package tflbikedownloader.domain.weather.weatherdata.wind;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class encapsulating wind-related information of a weather feed
 *
 * Created by Gabriel on 10/20/2014.
 */
@XmlRootElement(name = "wind")
public class WindNode {

    private SpeedNode speed;
    private DirectionNode direction;

    public SpeedNode getSpeed() {
        return speed;
    }

    @XmlElement(name = "speed")
    public void setSpeed(SpeedNode speed) {
        this.speed = speed;
    }

    public DirectionNode getDirection() {
        return direction;
    }

    @XmlElement(name = "direction")
    public void setDirection(DirectionNode direction) {
        this.direction = direction;
    }

}
