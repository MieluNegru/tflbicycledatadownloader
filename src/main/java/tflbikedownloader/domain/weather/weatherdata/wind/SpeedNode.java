package tflbikedownloader.domain.weather.weatherdata.wind;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Java bean holding wind speed-related information
 *
 * Created by Gabriel on 10/20/2014.
 */
public class SpeedNode {

    // wind speed expressed in mps
    private double value;

    // readable description of wind, e.g. "Gentle Breeze"
    private String name;

    public double getValue() {
        return value;
    }

    @XmlAttribute(name = "value")
    public void setValue(double value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @XmlAttribute(name = "name")
    public void setName(String name) {
        this.name = name;
    }
}
