package tflbikedownloader.domain.weather.weatherdata.city;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Bean encapsulating geographical coordinates of a city: latitude and longitude
 *
 * Created by Gabriel on 10/19/2014.
 */
public class CoordinatesNode {

    private double latitude;
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    @XmlAttribute(name = "lat")
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @XmlAttribute(name = "lon")
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
