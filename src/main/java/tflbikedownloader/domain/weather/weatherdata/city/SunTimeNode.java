package tflbikedownloader.domain.weather.weatherdata.city;

import javax.xml.bind.annotation.XmlAttribute;
import java.util.Date;

/**
 * Java bean holding information about sunrise and sunset time for a city on a particular day
 *
 * Created by Gabriel on 10/19/2014.
 */
public class SunTimeNode {

    private Date sunriseTime;
    private Date sunsetTime;

    public Date getSunriseTime() {
        return sunriseTime;
    }

    @XmlAttribute(name = "rise")
    public void setSunriseTime(Date sunriseTime) {
        this.sunriseTime = sunriseTime;
    }

    public Date getSunsetTime() {
        return sunsetTime;
    }

    @XmlAttribute(name = "set")
    public void setSunsetTime(Date sunsetTime) {
        this.sunsetTime = sunsetTime;
    }
}
