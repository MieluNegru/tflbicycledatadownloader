package tflbikedownloader.domain.weather.weatherdata.city;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Holds information about the city for which the weather data is generated
 *
 * Created by Gabriel on 10/19/2014.
 */
@XmlRootElement(name = "city")
public class CityNode {

    // unique id of the city
    private int id;

    // name of the city
    private String name;

    // the city's geographical coordinates
    private CoordinatesNode coordinates;

    // code of the city's countryCode
    private String countryCode;

    // time of sunrise and sunset
    private SunTimeNode sunTime;

    public int getId() {
        return id;
    }

    @XmlAttribute(name = "id")
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlAttribute(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public CoordinatesNode getCoordinates() {
        return coordinates;
    }

    @XmlElement(name = "coord")
    public void setCoordinates(CoordinatesNode coordinates) {
        this.coordinates = coordinates;
    }

    public String getCountryCode() {
        return countryCode;
    }

    @XmlElement(name = "country")
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public SunTimeNode getSunTime() {
        return sunTime;
    }

    @XmlElement(name = "sun")
    public void setSunTime(SunTimeNode sunTime) {
        this.sunTime = sunTime;
    }
}
