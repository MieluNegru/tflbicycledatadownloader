package tflbikedownloader.domain.weather.weatherdata;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Java bean used for unmarshalling data from XML node in the format <tagName name="foo" value="bar" />
 * <p>
 * Created by Gabriel on 10/20/2014.
 */
public class NameValueNode {

    private String name;
    private double value;

    public String getName() {
        return name;
    }

    @XmlAttribute(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    @XmlAttribute(name = "value")
    public void setValue(double value) {
        this.value = value;
    }

}
