package tflbikedownloader.domain.weather.weatherdata;

import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 *
 * Created by Gabriel on 10/20/2014.
 */
public class WeatherNode {

    private int number;
    private String value;
    private String icon;

    public int getNumber() {
        return number;
    }

    @XmlAttribute(name = "number")
    public void setNumber(int number) {
        this.number = number;
    }

    public String getValue() {
        return value;
    }

    @XmlAttribute(name = "value")
    public void setValue(String value) {
        this.value = value;
    }

    public String getIcon() {
        return icon;
    }

    @XmlAttribute(name = "icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }
}
