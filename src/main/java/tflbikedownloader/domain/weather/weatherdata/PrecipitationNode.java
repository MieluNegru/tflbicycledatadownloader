package tflbikedownloader.domain.weather.weatherdata;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by Gabriel on 10/20/2014.
 */
public class PrecipitationNode {

    private String mode;

    public String getMode() {
        return mode;
    }

    @XmlAttribute(name = "mode")
    public void setMode(String mode) {
        this.mode = mode;
    }
}
