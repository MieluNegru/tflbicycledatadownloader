package tflbikedownloader.domain.weather.weatherdata;

import javax.xml.bind.annotation.XmlAttribute;
import java.util.Date;

/**
 * Created by Gabriel on 10/20/2014.
 */
public class LastUpdateNode {

    private Date value;

    public Date getValue() {
        return value;
    }

    @XmlAttribute(name = "value")
    public void setValue(Date value) {
        this.value = value;
    }
}
