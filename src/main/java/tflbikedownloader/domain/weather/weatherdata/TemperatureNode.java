package tflbikedownloader.domain.weather.weatherdata;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class encapsulating temperature values in the feed at a given point.
 * <p/>
 * Stores current temperature, as well as minimum and maximum temperature of current day.
 * <p/>
 * Created by Gabriel on 10/19/2014.
 */
@XmlRootElement(name = "temperature")
public class TemperatureNode {

    // current temperature
    private double value;

    // minimum temperature
    private double minimum;

    // maximum temperature
    private double maximum;

    // unit in which temperature is expressed (i.e. Kelvin)
    private String unit;

    public double getValue() {
        return value;
    }

    @XmlAttribute(name = "value")
    public void setValue(double value) {
        this.value = value;
    }

    public double getMinimum() {
        return minimum;
    }

    @XmlAttribute(name = "min")
    public void setMinimum(double minimum) {
        this.minimum = minimum;
    }

    public double getMaximum() {
        return maximum;
    }

    @XmlAttribute(name = "max")
    public void setMaximum(double maximum) {
        this.maximum = maximum;
    }

    public String getUnit() {
        return unit;
    }

    @XmlAttribute(name = "unit")
    public void setUnit(String unit) {
        this.unit = unit;
    }
}
