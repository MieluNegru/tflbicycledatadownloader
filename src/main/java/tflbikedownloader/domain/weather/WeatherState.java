package tflbikedownloader.domain.weather;

import tflbikedownloader.domain.weather.weatherdata.*;
import tflbikedownloader.domain.weather.weatherdata.city.CityNode;
import tflbikedownloader.domain.weather.weatherdata.wind.WindNode;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Created by Gabriel on 10/18/2014.
 */
@SuppressWarnings("unused")
@XmlRootElement(name = "current")
public class WeatherState {

    private CityNode city;
    private TemperatureNode temperatureNode;
    private UnitValueNode humidity;
    private UnitValueNode pressure;
    private WindNode wind;
    private NameValueNode clouds;
    private PrecipitationNode precipitation;
    private WeatherNode weather;
    private LastUpdateNode lastUpdate;

    public CityNode getCity() {
        return city;
    }

    @XmlElement(name = "city")
    public void setCity(CityNode city) {
        this.city = city;
    }

    public TemperatureNode getTemperatureNode() {
        return temperatureNode;
    }

    @XmlElement(name = "temperature")
    public void setTemperatureNode(TemperatureNode temperatureNode) {
        this.temperatureNode = temperatureNode;
    }

    public UnitValueNode getHumidity() {
        return humidity;
    }

    @XmlElement(name = "humidity")
    public void setHumidity(UnitValueNode humidity) {
        this.humidity = humidity;
    }

    public UnitValueNode getPressure() {
        return pressure;
    }

    @XmlElement(name = "pressure")
    public void setPressure(UnitValueNode pressure) {
        this.pressure = pressure;
    }

    public WindNode getWind() {
        return wind;
    }

    @XmlElement(name = "wind")
    public void setWind(WindNode wind) {
        this.wind = wind;
    }

    public NameValueNode getClouds() {
        return clouds;
    }

    @XmlElement(name = "clouds")
    public void setClouds(NameValueNode clouds) {
        this.clouds = clouds;
    }

    public PrecipitationNode getPrecipitation() {
        return precipitation;
    }

    @XmlElement(name = "precipitation")
    public void setPrecipitation(PrecipitationNode precipitation) {
        this.precipitation = precipitation;
    }

    public WeatherNode getWeather() {
        return weather;
    }

    @XmlElement(name = "weather")
    public void setWeather(WeatherNode weather) {
        this.weather = weather;
    }

    public LastUpdateNode getLastUpdate() {
        return lastUpdate;
    }

    @XmlElement(name = "lastupdate")
    public void setLastUpdate(LastUpdateNode lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
