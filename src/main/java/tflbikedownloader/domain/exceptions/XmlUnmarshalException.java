package tflbikedownloader.domain.exceptions;

/**
 * Exception raised when attempting to unmarshal a XML document
 * <p/>
 * Created by Gabriel on 10/18/2014.
 */
public class XmlUnmarshalException extends RuntimeException {

    public XmlUnmarshalException(String message) {
        super(message);
    }

    public XmlUnmarshalException(String message, Exception cause) {
        super(message, cause);
    }
}
