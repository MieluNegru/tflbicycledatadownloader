package tflbikedownloader.util;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;

/**
 * Utility class containing convenience methods frequently used in unit tests
 *
 * Created by Gabriel on 10/20/2014.
 */
public class TestUtils {

    /**
     * Utility method for getting URL to resource file in the classpath
     *
     * @param filePath the classpath to the required file
     * @return URL to the file that needs retrieving
     * @throws IOException if getting url for {@code filePath} fails
     */
    public static String getLocalResourceFileUrl(String filePath) throws IOException {
        Resource resource = new ClassPathResource(filePath);
        return resource.getURL().toString();
    }
}
