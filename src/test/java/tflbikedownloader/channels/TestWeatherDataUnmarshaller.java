package tflbikedownloader.channels;

import org.junit.Assert;
import org.junit.Test;
import tflbikedownloader.domain.weather.weatherdata.TemperatureNode;
import tflbikedownloader.domain.weather.WeatherState;
import tflbikedownloader.domain.weather.weatherdata.city.CityNode;
import tflbikedownloader.domain.weather.weatherdata.wind.WindNode;
import tflbikedownloader.util.TestUtils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Unit test class for the weather data XML feed unmarshaller
 *
 * Created by Gabriel on 10/19/2014.
 */
public class TestWeatherDataUnmarshaller {

    /**
     * Test the unmarshalling of the &lt;city&gt; child node of the weather data XML document
     */
    @Test
    public void testCityUnmarshal() throws IOException, ParseException {
        String resourceUrl = TestUtils.getLocalResourceFileUrl("channels/city.xml");

        XmlDocumentUnmarshaller<CityNode> cityXmlDocumentUnmarshaller = new XmlDocumentUnmarshaller<>(resourceUrl, CityNode.class);
        CityNode city = cityXmlDocumentUnmarshaller.unmarshal();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        Assert.assertEquals(city.getId(), 2643743);
        Assert.assertEquals(city.getName(), "London");
        Assert.assertEquals(city.getCoordinates().getLatitude(), 51.51, 0.000001);
        Assert.assertEquals(city.getCoordinates().getLongitude(), -0.13, 0.000001);
        Assert.assertEquals(city.getCountryCode(), "GB");
        Assert.assertEquals(city.getSunTime().getSunriseTime(), dateFormat.parse("2014-10-18T06:30:29"));
        Assert.assertEquals(city.getSunTime().getSunsetTime(), dateFormat.parse("2014-10-18T17:00:42"));
    }

    /**
     * Test the unmarshalling of the &lt;temperature&gt; child node of the weather data XML document
     */
    @Test
    public void testTemperatureUnmarshal() throws IOException {
        String resourceUrl = TestUtils.getLocalResourceFileUrl("channels/temperature.xml");

        XmlDocumentUnmarshaller<TemperatureNode> cityXmlDocumentUnmarshaller = new XmlDocumentUnmarshaller<>(resourceUrl, TemperatureNode.class);
        TemperatureNode temperature = cityXmlDocumentUnmarshaller.unmarshal();

        Assert.assertEquals("kelvin", temperature.getUnit());
        Assert.assertEquals(290.9, temperature.getValue(), 0.000001);
        Assert.assertEquals(290.15, temperature.getMinimum(), 0.000001);
        Assert.assertEquals(291.15, temperature.getMaximum(), 0.000001);
    }

    /**
     * Test the unmarshalling of the &lt;wind&gt; child node of the weather data XML document
     */
    @Test
    public void testWindUnmarshal() throws IOException {
        String resourceUrl = TestUtils.getLocalResourceFileUrl("channels/wind.xml");

        XmlDocumentUnmarshaller<WindNode> windXmlDocumentUnmarshaller = new XmlDocumentUnmarshaller<>(resourceUrl, WindNode.class);
        WindNode wind = windXmlDocumentUnmarshaller.unmarshal();

        Assert.assertEquals(wind.getSpeed().getValue(), 4.1, 0.000001);
        Assert.assertEquals(wind.getSpeed().getName(), "Gentle Breeze");
        Assert.assertEquals(wind.getDirection().getValue(), 200, 0.000001);
        Assert.assertEquals(wind.getDirection().getCode(), "SSW");
        Assert.assertEquals(wind.getDirection().getName(), "South-southwest");
    }

    @Test
    public void testCurrentWeatherUnmarshal() throws IOException {
        String resourceUrl = TestUtils.getLocalResourceFileUrl("channels/weather.xml");

        XmlDocumentUnmarshaller<WeatherState> windXmlDocumentUnmarshaller = new XmlDocumentUnmarshaller<>(resourceUrl, WeatherState.class);
        WeatherState weatherState = windXmlDocumentUnmarshaller.unmarshal();

        // TODO add assertions to make sure all desired nodes were unmarshalled successfully
    }

}
