package tflbikedownloader.channels;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import tflbikedownloader.domain.tfl.BikeStationStateList;

import java.io.IOException;
import java.util.Properties;

/**
 * Unit test class for TFL live bicycle hire update feed unmarshaller
 *
 * Created by Gabriel on 10/18/2014.
 */
public class TestLiveCycleHireUpdateUnmarshaller {
    private XmlDocumentUnmarshaller<BikeStationStateList> unmarshaller;

    @Before
    public void init() throws IOException {
        Resource resource = new ClassPathResource("config/channels.properties");
        Properties props = PropertiesLoaderUtils.loadProperties(resource);

        String feedUrl = props.getProperty("channels.feed.url.bicycle");
        unmarshaller = new XmlDocumentUnmarshaller<>(feedUrl, BikeStationStateList.class);
    }

    /**
     * Test if unmarshalling the TFL live cycle hire update feed works
     */
    @Test
    public void testLiveCycleHireUnmarshal() {
        BikeStationStateList bikeStationStateList = unmarshaller.unmarshal();
        // error will be thrown if unmarshalling fails, failing the test
    }
}
